const { BlockTool } = require('./tools');
const crypto = require('crypto');

// Vous n'avez pas à comprendre BlockTool.
// Cette class vient en support du sujet.
// Si vous avez besoin de débugguer,
// vous pouvez commenter le `extends BlockTool`.
module.exports = class Block extends BlockTool {

  // Complétez le constructeur
  constructor(index, previous, data) {
    super() // Obligatoire car on hérite de BlockTool
    // Le mot clé `this` permet d'accèder aux propriétés de l'object depuis ses méthodes.
    this.index = index;
    this.data = data;
    this.previous = previous;
    this.id = this.getHash();
  }

  // Retourne l'identifiant du block en le calculant depuis les données
  getHash() {
    const toHash = `${this.index}${this.previous}${this.data}`;
    return crypto.createHash('sha256').update(toHash, 'utf8').digest('hex');
  }


  // Retourne un boolean qui indique si le block est valide
  isValid() {
    return this.getHash() === this.id;
  }

  // Utile à l'étape 2
  miner() {}
}
